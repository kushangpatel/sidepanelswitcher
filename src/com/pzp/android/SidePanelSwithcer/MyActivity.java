package com.pzp.android.SidePanelSwithcer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class MyActivity extends Activity {

    private Button buttonSwitch;
    private View subLayout;
    private View topLayout;
    private ListView subViewListView;
    private String listViewDummyContent[]={"Android","iPhone","BlackBerry","AndroidPeople"};
    private Display display;
    private View fakeLayout;
    private Animation.AnimationListener AL;

    // Values for after the animation
    private int oldLeft;
    private int oldTop;
    private int newleft;
    private int newTop;
    private int screenWidth;
    private int animToPostion;
    // TODO change the name of the animToPostion for a better explanation.

    private boolean menuOpen = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        bindResources();
        bindListeners();
        bindAnimationListeners();
        initListView();

        display =  getWindowManager().getDefaultDisplay();
        screenWidth = display.getWidth();
        int calcAnimationPosition = (screenWidth /3);

        // Value where the onTop Layer has to animate
        // also the max width of the layout underneath 
        // Set Layout params for subLayout according to calculation
        animToPostion = screenWidth - calcAnimationPosition;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(animToPostion, RelativeLayout.LayoutParams.FILL_PARENT);
        subLayout.setLayoutParams(params);
    }

    private void bindResources() {
        buttonSwitch = (Button)findViewById(R.id.button);
        subLayout = (View) findViewById(R.id.layout);
        topLayout = (View) findViewById(R.id.layoutTwo);
        subViewListView=(ListView)findViewById(R.id.listView1);
        fakeLayout = (View)findViewById(R.id.fake_layouy);
    }

    private void bindListeners() {
        topLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (menuOpen == true) {
                        animSlideLeft();
                    }
                }

                return false;
            }
        });

        buttonSwitch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(menuOpen == false){
                    animSlideRight();
                } else if (menuOpen == true) {
                    animSlideLeft();
                }
            }
        });

    }

    private void bindAnimationListeners() {
        AL = new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                buttonSwitch.setClickable(false);
                topLayout.setEnabled(false);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(menuOpen == true) {

                    topLayout.layout(oldLeft, oldTop, oldLeft + topLayout.getMeasuredWidth(), oldTop + topLayout.getMeasuredHeight() );
                    menuOpen = false;
                    buttonSwitch.setClickable(true);
                    topLayout.setEnabled(true);
                } else if(menuOpen == false) {

                    topLayout.layout(newleft, newTop, newleft + topLayout.getMeasuredWidth(), newTop + topLayout.getMeasuredHeight() );
                    topLayout.setEnabled(true);
                    menuOpen = true;
                    buttonSwitch.setClickable(true);
                }
            }
        };
    }

    private void initListView() {
        subViewListView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1 , listViewDummyContent));
    }

    private void animSlideRight(){

        fakeLayout.setVisibility(View.VISIBLE);
        newleft = topLayout.getLeft() + animToPostion;
        newTop = topLayout.getTop();
        TranslateAnimation slideRight = new TranslateAnimation(0,newleft,0,0);
        slideRight.setDuration(500);
        slideRight.setFillEnabled(true);
        slideRight.setAnimationListener(AL);
        topLayout.startAnimation(slideRight);
    }

    private void animSlideLeft() {

        fakeLayout.setVisibility(View.GONE);
        oldLeft = topLayout.getLeft() - animToPostion;
        oldTop = topLayout.getTop();
        TranslateAnimation slideLeft = new TranslateAnimation(newleft,oldLeft,0,0);
        slideLeft.setDuration(500);
        slideLeft.setFillEnabled(true);
        slideLeft.setAnimationListener(AL);
        topLayout.startAnimation(slideLeft);
    }
}  